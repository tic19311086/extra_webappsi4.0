# Proyect Development

---

## FORK this monorepo to Create the project products of work.

---

### Project Work Statement

#### Create a web solution, which architecture could be decoupled and reused in front and back; to acomplish the management of alumni subscriptions and payments of a code academy.

---

- The documentation, diagrams and code must be created and stored in the correspondent folder in the monorepo.
- The alumni could be subscribed to code courses with 15 days trial, without payment. After this trial, the payment must be meet in the next login to the course.
- The alumni could be susbcribed to courses in different inscription dates.
- Each course has an own price. The data of courses:
  - Id
  - Name
  - Description
  - Duration
  - Price
  - Level (enum('BASIC', 'INTERMEDIATE', 'ADVANCED'))
- The course subscription and payment deliver a mail message to the alumni as confirmation.
- Users module, with all users action, as login, are mandatory.

---

1. Create the requirements statement.
   1. Commit in a docs branch
1. Design the objects models in UML
   1. Commit in a uml branch
1. Design the Data Models in DB
   1. Commit in data branch
1. Design your UI in FIGMA and save your mocks in UI (OPTIONAL)
   1. Commit in ui branch
1. Create your backend basecode and implement your design
   1. Commit every FEATURE in every FEATURE branch needed
1. Publish and expose your API Endpoints
   1. Commit a published_api branch
1. Create your frontand basecode and consume your API endpoints and implement UI design.
   1. Commit in FEATURE branch as needed
1. Integrate your solution and commit in 'DEPLOY' branch the whole solution.

(Create your Pull Requests, AND MERGE TO 'development'; as you accomplish every stage in your project)
