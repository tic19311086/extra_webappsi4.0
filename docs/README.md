# Proyect Documentation

---

## Create the project documentation to support design.

---

### Project Work Statement

#### Create a web solution, which architecture could be decoupled and reused in front and back; to acomplish the management of alumni subscriptions and payments of a code academy.

---

- The documentation, diagrams and code must be created and stored in the correspondent folder in the monorepo.
- The alumni could be subscribed to code courses with 15 days trial, without payment. After this trial, the payment must be meet in the next login to the course.
- The alumni could be susbcribed to courses in different inscription dates.
- Each course has an own price. The data of courses:
  - Id
  - Name
  - Description
  - Duration
  - Price
  - Level (enum('BASIC', 'INTERMEDIATE', 'ADVANCED'))
- The course subscription and payment deliver a mail message to the alumni as confirmation.
- Users module, with all users action, as login, are mandatory.

---

Create the requirements statement.
